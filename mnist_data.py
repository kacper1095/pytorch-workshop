import pickle
import gzip
from os.path import dirname, join


_default_name = join(dirname(__file__), "datasets", "mnist.pkl.gz")


def get_store(fname=_default_name):
    mnist_f = gzip.open(fname,'rb')
    train_set, valid_set, test_set = pickle.load(mnist_f, encoding='latin1')
    mnist_f.close()

    train_set = list(zip(*train_set))
    valid_set = list(zip(*valid_set))
    test_set = list(zip(*test_set))

    return train_set, valid_set, test_set


if __name__=="__main__":
    get_store()