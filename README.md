# PyTorch Workshop
![Logo](https://image.ibb.co/dbE3Cc/logo.png)

## Introduction

Small repo for PyTorch workshop prepared for one of the meetings of Medical.ml Student Association at University of Technology in Wrocław.

## Prerequisites
To run this repo correctly you will need few more things:

### Docker
You can easily install this using

> https://docs.docker.com/install/

### Docker-Compose
Same as above.
> https://docs.docker.com/compose/install/

And that's it!

## Running
1. `git clone https://gitlab.com/kacper1095/pytorch-workshop`
2. `cd pytorch-workshop`
3. `docker-compose up --build`
4. After 3. command you can just: `docker-compose up` next time

Now, 2 ports are available:
> localhost:8080 - jupyter notebook

> localhost:8081 - tensorboard

When you access `localhost:8080` you will be asked for a password. It's:
> wololo

Additional points for a person, who'll guess the origin of the password and why I put it there :).

## Author

Kacper Kania