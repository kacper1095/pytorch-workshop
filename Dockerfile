FROM continuumio/miniconda3

ENV APP_DIR /usr/src/app

RUN mkdir ${APP_DIR}
COPY . ${APP_DIR}
WORKDIR ${APP_DIR}

RUN apt-get update
RUN conda env create -f env.yml
RUN sh download_mnist.sh

EXPOSE 8081
EXPOSE 8080

RUN ["chmod", "+x", "entrypoint.sh"]

RUN mkdir /root/.jupyter
COPY src/jupyter_notebook_config.py /root/.jupyter/

CMD ["/usr/src/app/entrypoint.sh"]