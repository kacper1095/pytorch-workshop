#!/usr/bin/env bash
source activate pytorch-workshop
nohup jupyter notebook --notebook-dir='/usr/src/app/notebooks' --ip=* --port=8080 --allow-root &
tensorboard --logdir='/usr/src/app/logs' --port=8081